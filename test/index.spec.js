var expect = require('chai').expect;
var nock = require('nock');

var app = require('../index.js');

describe('おためし', () => {
  var id = '123AABC';

  it('IPアドレスっぽいもの', () => {

    nock(/10.123.45.6/)
      .get('/groups')
      .reply(200, {
        id: id
      });

    var requestUrl = 'http://10.123.45.6:80/groups';

    return app.get(requestUrl).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.body.id).to.equal(id);
    });
  });

  it('パスワード認証っぽいもの', () => {

    nock(/10.123.45.6/)
      .get('/projects')
      .reply(200, {
        id: id
      });

    var requestUrl = 'http://1234:5678@10.123.45.6:80/projects';

    return app.get(requestUrl).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.body.id).to.equal(id);
    });
  });

  it('ポート指定？', () => {

    nock(/10.123.45.6/)
      .get('/status')
      .reply(200, {
        id: id
      });

    var requestUrl = 'http://1234:5678@10.123.45.6:80/status';

    return app.get(requestUrl).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.body.id).to.equal(id);
    });
  });
});
