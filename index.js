var http = require('http');
var request = require('request');

var app = {};

app.get = function(url) {

  return new Promise((resolve) => {
    request(url, (error, response, body) => {
      resolve({
        status: response.statusCode,
        body: JSON.parse(body)
      });
    });
  });
};

module.exports = app;
